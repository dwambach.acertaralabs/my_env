^!d::
   topclip := clipboard

   Send, """ Name/desc{Enter}
   
   if (topclip == "python")
   {
        Send, Available functions:{Enter}
        Send, {Enter}
        Send, Available classes:{Enter}
   }
   else if (topclip == "class")
   {
        Send, Available functions:{Enter}
        Send, {Enter}
        Send, Public attributes:{Enter}
   }
   else if (topclip == "def")
   {
        ; note pycharm wanted to auto fill after Args/Returns thus the prepended '_,'' user should remove
        Send, _Args:{Enter}
        Send, {Enter}
        Send, _Returns:{Enter}
   }
   ; note - not sending the 2nd """ because a lot of editors auto fill it :P
   return
